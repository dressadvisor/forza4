/**
 * Created by chiaravarini on 22/04/17.
 */
public class Main{

    public static void main(String[] args) {
        Controller controller = new Controller(new Forza4Impl("src/forza4.pl"));
        new F4Gui(controller);
    }
}
