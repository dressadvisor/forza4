import java.io.FileInputStream
import java.util.Optional

import Player.Player
import Utils._
import alice.tuprolog.{Term, Theory}

/**
  * Created by Federica on 24/04/17.
  */


trait Forza4 {

  def getBoard: List[Term]

  def checkCompleted: Boolean

  def move(column: Int): (Int, Int, Player)

  def checkVictory: Boolean

  def getWinner: Option[Player]

  def getPlayer: Player

}

object Player extends Enumeration{
  type Player = Value
  val PlayerRed, PlayerYellow = Value


}

class Forza4Impl(fileName: String) extends Forza4{

  var turn: Player = Player.PlayerRed
  private val engine = mkPrologEngine(new Theory(new FileInputStream(fileName)))
  private var board: List[Term] = List[Term]()

  override def getBoard: List[Term] = board

  override def checkCompleted: Boolean = {
    val b: Term = board
    val goal = "check_completed(" + b + ")"
    solveWithSuccess(engine, goal)
  }

  override def move(column: Int): (Int, Int, Player) = {
    val b: Term = board
    val player: String = turn
    val goal = "move("+b+","+player+","+column+", NB )"

    val newBoard: Term = solveOneAndGetTerm(engine, goal, "NB")
    val goal1 ="get_move(  "+b+", "+newBoard+", (X,Y,PL))"

    if(solveWithSuccess(engine, goal1)){

      println("Aggiungo mossa!")
      val cellRow = solveOneAndGetTerm(engine, goal1, "Y")
      board = board :+ Term.createTerm("("+column+","+cellRow+","+player+")")
      (column,Integer.parseInt(cellRow.toString),player)

    } else {
      print("Colonna piena!")
      (0,0,Player.PlayerRed)
    }
  }

  override def checkVictory: Boolean = {
    val b: Term = board
    //victory(B,PL)
    val goal = "victory("+ b + ", X )"
    solveWithSuccess(engine,goal)
  }

  override def getWinner: Option[Player] = {
    val b: Term = board
    val goal = "victory("+ b + ", X )"
    val winner = solveOneAndGetTerm(engine,goal,"X")
    print("winner " + winner)
    Option(winner.toString)
  }

  override def getPlayer: Player = turn match{
    case Player.PlayerRed => turn = Player.PlayerYellow; Player.PlayerRed
    case _ => turn = Player.PlayerRed;  Player.PlayerYellow

  }
}
