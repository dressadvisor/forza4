% player_color(?namePlayer, ?coinColor)
player_color(p1, "red").
    player_color(p2, "yellow").


% cell(?Colum,?Row,?State)
cell(X,Y,PL):- X=<7, Y=<6, player_color(PL,_).

% victory(+Board, -Player).
victory(B,PL):- player_color(PL,_), horizontal(B,PL),!; vertical(B,PL),!; oblique(B,PL).

% horizontal(+Board, -Player). Verify horizontal victory
horizontal(B,PL):-  member((X1,Y1,PL),B), 
 	  			  	X2 is X1+1, member((X2,Y1,PL),B),
 					X3 is X2+1, member((X3,Y1,PL),B),
 					X4 is X3+1, member((X4,Y1,PL),B).

% vertical(+Board, -Player). Verify vertical victory
vertical(B,PL):- member((X1,Y1,PL),B), 
 	  			 Y2 is Y1+1, member((X1,Y2,PL),B),
 				 Y3 is Y2+1, member((X1,Y3,PL),B),
 				 Y4 is Y3+1, member((X1,Y4,PL),B).

% oblique(+Board, -Player). Verify oblique victory
oblique(B,PL):- oblique_right(B,PL); oblique_left(B,PL).

oblique_right(B,PL):- member((X1,Y1,PL),B), 
 	  			  	  Y2 is Y1+1,X2 is X1+1, member((X2,Y2,PL),B),
 					  Y3 is Y2+1,X3 is X2+1, member((X3,Y3,PL),B),
 					  Y4 is Y3+1,X4 is X3+1, member((X4,Y4,PL),B).
 								   
oblique_left(B,PL):- member((X1,Y1,PL),B), 
 	  			  	 Y2 is Y1+1,X2 is X1-1, member((X2,Y2,PL),B),
 					 Y3 is Y2+1,X3 is X2-1, member((X3,Y3,PL),B),
 					 Y4 is Y3+1,X4 is X3-1, member((X4,Y4,PL),B).

% move(+Board,+Player,+Column,-NewBoard)
move(B,PL,X,[(X,Y,PL)|B]):- count_coin(X,B,CC), CC<6,!, Y is CC+1.
move(B,PL,X,B).

% count_coin(+Column, +Board, -NumberOfColumnCells)  ~filter+size
count_coin(_,[],0).
count_coin(C, [(X1,_,_)|T], M):- C=:=X1,!, count_coin(C,T,N), M is N+1.
count_coin(C,[_|T], M):- count_coin(C,T,M).

%count_filtered_list(+Board,+Column,-NumberOfColumnCells)
%count_filtered_list(B,C,S):- findall(coin,member((C,_,_),B),OL), size(OL,S).

% check_completed(+Board)
check_completed(B):- size(B,42).

% size(+Board,-totalElementInBoard)
size([],0).
size([_|T],N):- size(T,N1), N is N1+1.

% get_move(+Board, +NewBoard, -lastCellAdded)
get_move([],[(X,Y,PL)], (X,Y,PL)).
get_move([H|T], [NH|NT], (X,Y,PL)):- H == NH, !, get_move(T,NT, (X,Y,PL)).
get_move(B, [(X,Y,PL)|_], (X,Y,PL)).