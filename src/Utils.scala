/**
  * Created by Federica on 25/04/17.
  */
import javax.swing.ImageIcon
import Player.Player
import alice.tuprolog._

object Utils {

  implicit def stringToTerm(s: String): Term = Term.createTerm(s)
  implicit def listToTerm[T](l: List[T]): Term = l.mkString("[",",","]")
  implicit def stringToTheory[T](s: String): Theory = new Theory(s)
  implicit def playerToString(player: Player): String = player match {
    case Player.PlayerRed => "p1"
    case _ => "p2"
  }
  implicit def stringToPlayer(s: String): Player = s match {
    case "p1" => Player.PlayerRed
    case _ => Player.PlayerYellow
  }

  def mkPrologEngine(theory: Theory): Term => Stream[SolveInfo] = {
    val engine = new Prolog
    engine.setTheory(theory)

    goal => new Iterable[SolveInfo]{

      override def iterator = new Iterator[SolveInfo]{
        var solution: Option[SolveInfo] = Some(engine.solve(goal))

        override def hasNext = solution.isDefined &&
          (solution.get.isSuccess || solution.get.hasOpenAlternatives)

        override def next() =
          try solution.get
          finally solution = if (solution.get.hasOpenAlternatives) Some(engine.solveNext()) else None
      }
    }.toStream
  }

  def solveWithSuccess(engine: Term => Stream[SolveInfo], goal: Term): Boolean =
    engine(goal).map(_.isSuccess).headOption == Some(true)

  def solveOneAndGetTerm(engine: Term => Stream[SolveInfo], goal: Term, term: String): Term =
    engine(goal).headOption map (extractTerm(_,term)) get

  def extractTerm(solveInfo:SolveInfo, i:Integer): Term =
    solveInfo.getSolution.asInstanceOf[Struct].getArg(i).getTerm

  def extractTerm(solveInfo:SolveInfo, s:String): Term =
    solveInfo.getTerm(s)

  def getImageIcon(nameImage: String): ImageIcon = new ImageIcon(getClass.getClassLoader.getResource("resources/"+nameImage))

  def getButtonCoinImage(nameImage: String): ImageIcon = {
    val coinDimension = 60
    val i = Utils.getImageIcon(nameImage)
    val newImg = i.getImage.getScaledInstance(coinDimension, coinDimension, java.awt.Image.SCALE_SMOOTH) // scale it the smooth way
    new ImageIcon(newImg)
  }
}
