/**
  * Created by Chiara on 25/04/17.
  */

import Player.Player

class Controller (val forza4:Forza4){

  def move(column: Int) : (Int, Int, Player) = forza4.move(column)

  def checkVictory : Boolean = forza4.checkVictory

  def getWinner : Option[Player] = forza4.getWinner

  def exit(): Unit = System.exit(0)

  def getPlayer: Player = forza4.getPlayer

  def checkCompleted: Boolean = forza4.checkCompleted

}