import javax.swing.*;
import java.awt.*;
import java.util.*;

/**
 * Created by Chiara on 23/04/17.
 */
public class F4Gui extends JFrame{

    private static final int N_ROWS = 6;
    private static final int N_COLUMNS = 7;

    private final JButton exit = new JButton("Exit");
    private final Set<Forza4Cell> board = new HashSet<Forza4Cell>();
    private final Controller controller;

    public F4Gui(final Controller controller) {
        this.controller = controller;
        this.initGui();
    }

    private void initGui(){

        this.setTitle("Forza4");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int)(d.getWidth()/3), (int)(d.getHeight()/2));
        this.setLayout(new BorderLayout());

        JPanel backgroundPanel=new BackgroundPanel();

        for (int i=N_ROWS;i>=1;i--){
            for (int j=1;j<=N_COLUMNS;j++){
                Forza4Cell cell = new Forza4Cell(j,i);
                board.add(cell);
                backgroundPanel.add(cell);
            }
        }

        JPanel s=new JPanel(new FlowLayout());
        s.add(exit);
        exit.addActionListener(e -> controller.exit());

        this.add(BorderLayout.CENTER,backgroundPanel);
        this.add(BorderLayout.SOUTH,s);

        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private class Forza4Cell extends JButton{
        private int column,row;

        private Forza4Cell(final int column, final int row){
            this.column = column;
            this.row = row;

            this.setBorderPainted(false);
            this.setContentAreaFilled(false);
            this.setFocusPainted(false);
            this.setOpaque(false);

            this.addActionListener(e->{

                int moveRow =  (int)controller.move(column)._2();

                if(moveRow>0) {
                    setButtonIcon(column, moveRow, controller.getPlayer().toString());
                    checkEnd();
                }
            });
        }

        private int getColumn() {
            return column;
        }
        private int getRow() {
            return row;
        }
    }

    private void setButtonIcon(int column, int row, String namePlayer){
        board.forEach(cell -> {
            if(cell.getColumn()==column && cell.getRow()==row){
                cell.setIcon(getButtonIcon(namePlayer));
            }
        });
    }

    private ImageIcon getButtonIcon(String namePlayer){
        if(namePlayer.equals(Player.PlayerRed().toString())) {
            return Utils.getButtonCoinImage( "red_coin.png");

        } else {
            return Utils.getButtonCoinImage("yellow_coin.png");
        }
    }


    private class BackgroundPanel extends JPanel{

        private BackgroundPanel(){
            setLayout(new GridLayout(N_ROWS,N_COLUMNS));
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            try {
                // Get the image and set it to the imageicon
                ImageIcon imageIcon = Utils.getImageIcon("game_board.png");
                g.drawImage(imageIcon.getImage(),0,0, this.getWidth(),this.getHeight(), Color.white, null);
            }
            catch(Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void checkEnd(){
        if(controller.checkVictory()) {
            exit.setText(controller.getWinner().get() + " won!");
            disableBoard();

        } else if (controller.checkCompleted()) {
            exit.setText("Even!");
            disableBoard();
        }

    }

    private void disableBoard(){
        board.forEach(cell -> Arrays.asList(cell.getActionListeners()).forEach(a->cell.removeActionListener(a)));
    }
}
